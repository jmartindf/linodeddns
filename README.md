# README #

A Python script to update DNS entries at Linode, using the Linode API.

## Config ##

Copy host.cfg.sample to the file of your choice and modify it. The file is in the [INI file format](https://en.wikipedia.org/wiki/INI_file#Format).

Edit the [global] section and put in your Linode API key.

Create one section for each host that you wish the client to update. For each host, you need to provide the Linode domain ID and resource ID. Right now, you'll need to find those on your own. In the future, I may create a tool to list out all of the domains and resources under your account.

You may, optionally, provide a 'type' setting for each host. If the type is ip4 (or 4 or ipv4), then the host will be updated with your current IPv4 address. If the type is ip6 (or 6 or ipv6), then the host will be updated with your current IPv6 address. The default is to use your IPv4 address.

## Credits ##

Linode API module taken from [https://github.com/tjfontaine/linode-python](https://github.com/tjfontaine/linode-python).