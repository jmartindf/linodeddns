#!/usr/bin/env python
import urllib2
from linode.api import Api
import ConfigParser, argparse
import sys

def doUpdates(config_file):
	api_key = ''
	ip4 = ""
	ip6 = ""
	should_update = False

	try:
		ip4 = urllib2.urlopen('http://remoteip.desertflood.com/').read().strip()
	except:
		pass
	try:
		ip6 = urllib2.urlopen('http://remoteip6.desertflood.com/').read().strip()
	except:
		pass

	if ip4 is None and ip6 is None:
		print("I couldn't look up any IP addresses.")
		sys.exit(-1)

	config = ConfigParser.SafeConfigParser({
			'current_ip': '0.0.0.0',
			'type': 'ip4'
		})
	try:
		config.readfp(open(config_file,'r'))
	except:
		print ("Couldn't parse config file")
		sys.exit(-1)

	try:
		api_key = config.get('global','api_key')
	except ConfigParser.NoOptionError:
		print ("You must specify an API key!")
		sys.exit(-1)

	api = Api(key=api_key)

	for host in config.sections():
		if host == 'global':
			continue
		ip = ""
		try:
			domain_id = config.get(host, 'domain_id')
			resource_id = config.get(host, 'resource_id')
		except ConfigParser.NoOptionError:
			print("Skipping %s due to missing information." % host)
			continue
		
		ip_type = config.get(host, 'type').lower()
		current_ip = config.get(host, 'current_ip')

		if ip_type=="4" or ip_type=="ipv4" or ip_type=="ip4":
			ip = ip4
		if ip_type=="6" or ip_type=="ipv6" or ip_type=="ip6":
			ip = ip6
		if ip != "" and current_ip != ip and domain_id != "" and resource_id != "":
			api.domain_resource_update(DomainID=domain_id,ResourceID=resource_id,target=ip)
			config.set(host,"current_ip",ip)
			should_update = True
			print "Updated %s to %s" % (host, ip)

	if should_update:
		try:
			config.write(open(config_file,"w"))
			print("Updated configuration file")
		except:
			print("Couldn't update configuration file.")

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Do Linode DDNS updates.')
	parser.add_argument('config', help='the config file to use')
	args = parser.parse_args()
	doUpdates(args.config)
